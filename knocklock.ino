
float[] lock;
float timeout = 3000;
float threshold = 0.05;
int piezo = 0;
int redLED = 0;
int greenLED = 0;
int button = 0;
int[] temp;
int knockThreshold = 0;
unsigned long lastKnockTime = 0;
int globalTimeout = 3000;
int timeout = 200;
int knockCounter = 0;
void setLock(unsigned long[] in){
  int[] dif = new int[in.length-1];
  for(int i = 0; i < in.length-1; i++){
    dif[i] = in[i+1] - in[i];
  }
  lock = scale(dif);
}

float[] scale(int[] dif){
  int maxTime = 0;
  for(int i:dif){
    if(i > maxTime){
      maxTime = i;
    }
  }
  float[] out = new float[dif.length];
  for(int i = 0; i<dif.length; i++}{
    out[i] = (float) dif[i] / (float) maxTime;
  }
  return out;
}
boolean compareToLock(float[] dif){
    if(dif.length != lock.length){
      return false;
    }
    for(int i = 0; i< dif.length; i++){
      if(abs(dif[i] -lock[i]) > threshold){
        return false;
      }
    }
    return true;
}

boolean processInput(unsigned long[] in){
  int[] dif = new int[in.lengrh-1];
  for(int i = 0; i < dif.length; i++){
    dif[i] = in[i+1] - in[i];
  }
  dif = scale(dif);
  return compareToLock(dif);
}
boolean knock(){
  if(analogRead(piezo) > knockThreshold && knockTimeout() > timeout){
    lastKnockTime = millis();
    return true;
  } else {
    return false;
  }
}
int knockTimeout(){
  return millis() - lastKnockTime;
}
void loop(){
  if(lock != null){
    listenForUnlock();
  }  
  if(buttonIsPressed()){
    temp = null;
    while (buttonIsPressed()){
      if(knock()){
        if(temp == null){
          temp = new unsigned long[10];
          knockCounter = 0;
        }
        temp[knockCounter] = millis();
        knockCounter ++;
      }
    }
    unsigned long[] newLock = new unsigned long[knockCounter];
    for(int i = 0; i<knockCounter; i++){
      newLock[i] = temp[i];
    }
    setLock(newLock);
    temp = null;
    knockCounter = 0;
  }
}
boolean buttonIsPressed(){
  if(readAnalog(button) > 700){
    return true;
  } else {
    return false;
  }
}
void listenForUnlock(){
  if(knockCounter >= lock.length){
    processInput(temp);
    temp = null;
  }
  if(knock()){
    if(temp == null){
      temp = new unsigned long[lock.length];
      knockCounter = 0;
    }
    temp[knockCounter] = millis();
    knockCounter ++;
  }
  if(knockTimeout() > globalTimeout){
    temp = null;
    knockCounter = 0;
  }
}
void setup(){
  }
